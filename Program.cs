﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace StructProductShop
{
    class Program
    {
        static int CURRENT_ID = 0;
        struct game
        {
            public int id;
            public string name;
            public int kollich;
            public char symbol;
            public double reyting;
        }


        static void ResizeArray(ref game[] games, int NewLength)
        {
            int minLength = NewLength > games.Length ? games.Length : NewLength;

            game[] newGames = new game[NewLength];

            for (int i = 0; i < minLength; i++)
            {
                newGames[i] = games[i];
            }

            games = newGames;

        }
        static void Main(string[] args)
        {
            game[] games = null;
            bool playGame = true;

            do
            {
                Console.Clear();
                printAllGames(games);

                PrintMenu();
                int MenuPoint = inputInt("Введите пункт меню: ");

                switch (MenuPoint)
                {
                    case 1:
                        {
                            game game = createNewElement();
                            addNewElement(ref games, game);
                            continue;
                        }
                        break;
                    case 2:
                        {
                            printGames(games);
                        }
                        break;
                    case 3:
                        {
                            Console.Write("Введите директорию для сохранения: ");
                            string fileName = Console.ReadLine();
                            WriteAllGames(games, fileName);
                            Console.ReadKey();
                        }
                        break;
                    case 4:
                        {
                            SortGames(games);
                        }
                        break;
                    case 5:
                        {
                            game[] tempGames = minMaxSearch(games);
                            printAllGames(tempGames);
                            Console.ReadKey();
                        }
                        break;
                    case 0:
                        {
                            playGame = false;
                            Console.Write("Работа завершена");
                        }
                        break;
                    default:
                        break;
                }


            } while (playGame);


            Console.ReadKey();
        }

        private static game[] minMaxSearch(game[] games)
        {
            Console.Write("Введите минимальный рейтинг: ");
            double tempMin = double.Parse(Console.ReadLine());

            Console.Write("Введите максимальный рейтинг: ");
            double tempMax = double.Parse(Console.ReadLine());

            game[] mas = new game[0];

            for (int i = 0; i < games.Length; i++)
            {
                if (games[i].reyting > tempMin && games[i].reyting < tempMax)
                {
                    addNewElement(ref mas, games[i]);
                }

            }
            return mas;
        }

        private static game SortGames(game[] games)
        {
            game temp = new game();

            for (int i = 0; i < games.Length - 1; i++)
            {
                for (int j = 0; j < games.Length - i - 1; j++)
                {
                    if (games[j + 1].kollich > games[j].kollich)
                    {
                        temp = games[j + 1];
                        games[j + 1] = games[j];
                        games[j] = temp;
                    }
                }
            }
            return temp;
        }

        private static void WriteAllGames(game[] games, string fileName)
        {
            /*string writePath = @"D:\Games.txt";

            string text = "";

            for (int i = 0; i < games.Length; i++)
            {
                text = text + $"{games[i].name}{Environment.NewLine}";
            }
            using (StreamWriter sw = new StreamWriter(writePath, false, System.Text.Encoding.Default))
            {
                sw.WriteLine(text);

                sw.Close();
            }*/ //тоже рабочий
            
            StreamWriter writer = new StreamWriter(fileName);

            writer.WriteLine(games.Length);
            writer.WriteLine(CURRENT_ID);

            for (int i = 0; i < games.Length; i++)
            {
                writer.WriteLine(games[i].id);
                writer.WriteLine(games[i].name);
            }

            writer.Close();
        
        Console.WriteLine("Запись выполнена");

        }

        private static void printGames(game[] games)
        {
            if (games == null)
            {
                Console.WriteLine("Игр нет");
            }
            else
            {

                for (int i = 0; i < games.Length; i++)
                {
                    Console.WriteLine(games[i].name);
                }
            }
            Console.ReadKey();
        }

        private static void printAllGames(game[] games)
        {
            Console.WriteLine("{0,-3}{1,-15}{2,-15}{3,-12}{4,-4}", "ИД", "Название", "Колличество", "Качество", "Рейтинг");
            if (games == null)
            {
                Console.WriteLine("Игр нет");
            }
            else
            {
                for (int i = 0; i < games.Length; i++)
                {
                    Console.WriteLine("{0,-3}{1,-15}{2,-15}{3,-12}{4,-4}", games[i].id, games[i].name, games[i].kollich, games[i].symbol, games[i].reyting);
                }
            }
            Console.WriteLine("--------------------------------------------------------------");
        }

        private static void addNewElement(ref game[] games, game game)
        {
            if (games == null)
            {
                games = new game[1];
            }
            else
            {
                ResizeArray(ref games, games.Length + 1);
            }
            games[games.Length - 1] = game;
        }

        private static game createNewElement()
        {
            game game;
            CURRENT_ID++;
            game.id = CURRENT_ID;

            Console.Write("Введите название игры: ");
            game.name = Console.ReadLine();

            Console.Write("Введите колличество скачиваний: ");
            game.kollich = int.Parse(Console.ReadLine());

            Console.Write("Введите качество игры(A,B,C): ");
            game.symbol = char.Parse(Console.ReadLine());

            Console.Write("Введите рейтинг игры (от 0,1 до 2): ");
            game.reyting = double.Parse(Console.ReadLine());

            return game;
        }

        private static int inputInt(string message)
        {
            Console.Write(message);
            int inputInt = int.Parse(Console.ReadLine());

            return inputInt;
        }

        private static void PrintMenu()
        {
            Console.WriteLine("1. Добавить новый элемент");
            Console.WriteLine("2. Вывести все игры на экран");
            Console.WriteLine("3. Сохранить все");
            Console.WriteLine("4. сортировать игры по колличеству скачиваний");
            Console.WriteLine("5. поиск игр с мин по макс рейтинг");
            Console.WriteLine("0. завершение работы");
        }
    }
}
